import csv
import pickle

def calcular_cuotas(archivo_entrada, archivo_salida, valor_cuota):
    ''' Ejercicio 1 '''
    with open(archivo_entrada, 'r') as entrada, open(archivo_salida, 'w') as salida:
        escritor = csv.writer(salida, quoting=csv.QUOTE_NONNUMERIC)
        escritor.writerow(["Nombre", "Nro de Socio", "Categoría", "Cantidad de cuotas", "Importe"])
        lector = csv.reader(entrada)
        next(lector)
        for nombre, nro_socio, categoria, cuotas_pendientes in lector:
            cantidad_cuotas = int(cuotas_pendientes) + 1
            try:
                fila = [nombre, nro_socio, categoria, cantidad_cuotas, cantidad_cuotas * valor_cuota[categoria]]
            except KeyError:
                print('"%s", "%s", "%s" categoría inexistente' % (nombre, nro_socio, categoria))
            else:
                escritor.writerow(fila)

def guardar_datos_faltan_en_disco(faltan_datos, archivo):
    ''' Ejercicio 2.ii '''
    with open(archivo, 'wb') as salida:
        pickle.dump(faltan_datos, salida)


def recuperar_datos_faltan_en_disco(archivo):
    ''' Ejercicio 2.iii '''
    with open(archivo, 'rb') as entrada:
        print(pickle.load(entrada))