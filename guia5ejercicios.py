

def listaPalabrasDicFrec(listaPalabras):
	frecuenciaPalab = [listaPalabras.count(p) for p in listaPalabras]
	diccionarioPalabras = dict(zip(listaPalabras,frecuenciaPalab))
	return diccionarioPalabras


cadenaPalabras = 'it was the best of times it was the worst of times '
cadenaPalabras += 'it was the age of wisdom it was the age of foolishness'
listaPalabras = cadenaPalabras.split()
eso = listaPalabrasDicFrec(listaPalabras)
print(eso)

print("\n Ejercicio 1: \n")

documentos = {"doc1": "La casa está ordenada", "doc2": "María se casa el día de mañana",
"doc3": "María es muy ordenada", "doc4": "Mañana será un gran día"}

documentos2 = {"doc1": "Son las cinco menos cinco", "doc2": "Faltan cinco para las cinco", "doc3": "Cuantas veces dije cinco", "doc4": "Sin contar el último cinco."}

def contarPalabrasEnDiccionarios(dic):
	dicPalabras = {}
	for keys in dic:
		string = dic[keys].split()
		nuevoDic = listaPalabrasDicFrec(string)
		print(nuevoDic)
		for keys2 in nuevoDic:
			value2 = nuevoDic[keys2]
			if(dicPalabras.get(keys2, 0) == 0):
				dicPalabras[keys2] = value2
			else:
				value = dicPalabras[keys2]
				if nuevoDic[keys2] > 1:
					dicPalabras[keys2] = value2 + value
				else:
					dicPalabras[keys2] = value + 1
	return dicPalabras

documentosPalabras = contarPalabrasEnDiccionarios(documentos)

documentos2Palabras = contarPalabrasEnDiccionarios(documentos2)

print(documentosPalabras)
print("\n")
print(documentos2Palabras)

print("\n Ejercicio 2: \n")

def listarDocumentosEnlosQueAparece(dic):
	dicDocumentos = {}
	for keys in dic:
		string = dic[keys].split()
		for p in string:
			if(dicDocumentos.get(p, 0) == 0):
				dicDocumentos[p] = [keys]
			else:
				dicDocumentos[p].append(keys)
	return dicDocumentos

listaDocs = listarDocumentosEnlosQueAparece(documentos)

print(listaDocs)

print("\n Ejercicio 3: \n")

def buscarDocumentosCoincidentesDeDosPalabras(docs, key1, key2):
	buscar = listarDocumentosEnlosQueAparece(docs)
	resultado = []
	if buscar.get(key1, 0) != 0:
		if buscar.get(key2, 0) != 0:
			for doc in buscar[key2]:
				if doc in buscar[key1]:
					if(not doc in resultado):
						resultado.append(doc)
		else:
			print("No existe " + key2)
	else:
		print("No existe " + key1)
	return resultado

def buscarDocumentosQueIncluyenUnaUOtraPalabra(docs, key1, key2):
	buscar = listarDocumentosEnlosQueAparece(docs)
	resultado = []
	if buscar.get(key1, 0) != 0:
		if buscar.get(key2, 0) != 0:
			for doc in buscar[key2]:
				if(not doc in resultado):
					resultado.append(doc)
		else:
			print("No existe " + key2)
		for doc in buscar[key1]:
				if(not doc in resultado):
					resultado.append(doc)
	else:
		print("No existe " + key1)
	return resultado

def buscarDocumentosQueNoContienenUnaPalabra(docs, key1):
	buscar = listarDocumentosEnlosQueAparece(docs)
	resultado = []
	for documento in docs:
		resultado.append(documento)
	for docs in buscar:
		if buscar.get(key1, 0) != 0:
			for doc in buscar[key1]:
				if(doc in resultado):
					resultado.remove(doc)
		else:
			print("No existe " + key1)
	return resultado


listaDocsCoincidentes = buscarDocumentosCoincidentesDeDosPalabras(documentos, "día", "mañana")
listaDocsQueContienenUnaUOtra = buscarDocumentosQueIncluyenUnaUOtraPalabra(documentos, "día", "mañana")
documentosQueNoContienenUnaPalabra = buscarDocumentosQueNoContienenUnaPalabra(documentos, "día")
print(documentos)
print("\n Lista de documentos que contienen día y mañana:\n")
print(listaDocsCoincidentes)
print("\n Lista de documentos que contienen día o mañana:\n")
print(listaDocsQueContienenUnaUOtra)
print("\n Lista de documentos que no contienen día:\n")
print(documentosQueNoContienenUnaPalabra)
print("\n Lista de documentos que contienen María o día pero no ordenada: \n")

