import csv
import re

def crearCsvEntradaParaEjercicio1(archivo):
	o_file = open(archivo + '.csv', 'w', newline= '')
	writer = csv.writer(o_file, delimiter=';')
	writer.writerow(('Escuela', 'Localidad', 'Vacantes'))
	writer.writerow(('Nro 25', 'Caseros', '43'))
	writer.writerow(('Nro 32', 'Santos Lugares', '12'))
	writer.writerow(('Nro 1', 'Caseros', '5'))
	o_file.close()

def mapper(archivo_entrada, archivo_salida):
	with open (archivo_entrada + '.csv', 'r') as archivo:
		reader = csv.reader(archivo, delimiter = ';')
		lista = []
		for row in reader:
			linea = " ".join(row)
			for palabra in row:
				datos = (palabra, 1)
				lista.append(datos)
		lista.sort()
	with open (archivo_salida + '.csv', 'w', newline= '') as salida:
		writer = csv.writer(salida, delimiter = ';')
		for dato in lista:
			writer.writerow(dato)

def reducer(archivo_entrada, archivo_salida):
	with open (archivo_entrada + '.csv', 'r') as archivo:
		reader = csv.reader(archivo, delimiter = ';')
		dicPalabras = {}
		for row in reader:
			palabra = row[0]
			cantidad = row[1]
			if dicPalabras.get(palabra) is None:
				dicPalabras[palabra] = int(cantidad)
			else: 
				actual = dicPalabras[palabra]
				dicPalabras[palabra] = actual + int(cantidad)
	with open (archivo_salida + '.csv', 'w', newline='') as salida:
		writer = csv.writer(salida, delimiter= ';')
		for palabra in dicPalabras:
			dato = (palabra, dicPalabras[palabra])
			writer.writerow(dato)


def mapper2(cadena):
	lista = []
	cantidad = 1
	terminado = False
	while not terminado:
		if cantidad == 11:
			length = "11,"
			terminado = True
		else:
			length = str(cantidad)
		expresion = ('(?:(?<=\W)|(?<=^))[\w]{' + length + '}(?=\s|$)')
		funcion = re.compile(expresion)
		res = funcion.finditer(cadena)
		if res:
			for item in res:
				print(cantidad)
				print(item.group())
		cantidad = cantidad + 1
