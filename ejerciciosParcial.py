class ErrorBase (Exception):
	"""Clase base de excepciones"""
	pass 

class CarreraDesconocidaError(ErrorBase):
	pass

class LegajoDesconocidoError(ErrorBase):
	pass

import csv
import pickle

"""MODELO 1"""
"""EJERCICIO 1"""

""" 1. Escribir una función Python completar_dirección que recibe el nombre de dos archivos csv_entrada,
csv_salida y un diccionario de clientes con clave nombre y como dato la dirección.
El archivo CSV de entrada contiene datos de pedidos de delivery con formato
“Nombre”, “Telefono”, “Entrega”, “A cobrar”
“Ana”, “1566778899”, “Paquete 3 pizzas”, “120”,
“Juan”, “1566778800”, “Pancho y coca”, “65”
“Pepe”, “1544778899”, “3 empanadas”, “60”
La salida debe contener además la dirección del cliente (o “Dirección desconocida” si no se encuentra el
nombre en el diccionario).
Si el diccionario de clientes es {“Ana”: “Valentín Gómez 4733”, “Juan”: “Caseros 3033” } la salida será
“Nombre”, “Telefono”, “Entrega”, “A cobrar”, “Dirección”
“Ana”, “1566778899”, “Paquete 3 pizzas”, “120”, “Valentín Gómez 4733”
“Juan”, “1566778800”, “Pancho y coca”, “65”, “Caseros 3033”
“Pepe”, “1544778899”, “3 empanadas”, “60”, “Dirección desconocida” """

dic_clientes = {"Ana": "Valentin Gomez 4733", "Juan": "Caseros 3033"}

def crearCsvEntradaParaEjercicio1(csv_entrada):
	o_file = open(csv_entrada + '.csv', 'w', newline= '')
	writer = csv.writer(o_file,quoting=csv.QUOTE_NONNUMERIC, delimiter=';')
	writer.writerow(('Nombre', 'Telefono', 'Entrega', 'A cobrar'))
	writer.writerow(('Ana', 1566778899, 'Paquete 3 pizzas', 120))
	writer.writerow(('Juan', 1566778800, 'Pancho y coca', 65))
	writer.writerow(('Pepe', 1544778899, '3 empanadas', 60))
	o_file.close()

def completar_direccion(archivo_entrada, archivo_salida, clientes):
	i_file = open(archivo_entrada + '.csv', 'r')
	reader = csv.reader(i_file, delimiter=";")
	next(reader)
	o_file = open(archivo_salida + '.csv', 'w', newline= '')
	writer = csv.writer(o_file,quoting=csv.QUOTE_NONNUMERIC, delimiter=';')
	writer.writerow(('Nombre', 'Telefono', 'Entrega', 'A cobrar', 'Direccion'))
	for row in reader:
		nombre = row[0]
		telefono = int(row[1])
		entrega = row[2]
		acobrar = int(row[3])
		if(clientes.get(nombre, 0) is 0):
			direccion = "Direccion desconocida"
		else:
			direccion = clientes[nombre]
		writer.writerow([nombre, telefono, entrega, acobrar, direccion])
	i_file.close()
	o_file.close()

"""EJERCICIO 2"""

"""2. Se tiene un diccionario carreras que tiene como clave el nombre de una carrera y como contenido una lista
de materias. Por ejemplo, podemos tener:

carreras = { “música”: [“historia”, “piano 1”, “piano 2”, “guitarra”, “composición”],
“pintura”: [“historia”, “dibujo 1”, “dibujo 2”, “pintura 1”, “pintura 2”] }

Se tiene también un diccionario alumnos, cuya clave es el legajo y como datos una tupla con nombre, lista de
carreras en las que está inscripto y un diccionario con clave nombre de la materia y lista de notas (puede
aparecer más de una nota solamente si el alumno no aprobó la materia la primera vez). Por ejemplo:

alumnos = {1000: ("Ana", ["música", "pintura"], {"historia": [2, 10], "piano 1": [8], "piano 2": [2]}),
2000: ("Juan", ["pintura"], {"historia": [6], "dibujo 1": [8], "pintura 1": [2, 7], "dibujo 2": [2]})}

i. Para ayudar al Departamento de Alumnos se debe escribir una función Python que_le_falta que recibe los
diccionarios carreras y alumnos, un número de legajo y una carrera como datos, y devuelve una lista con las
materias que le faltan a ese legajo para completar esa carrera.
Si la carrera no existe se debe lanzar la excepción CarreraDesconocidaError (definirla adecuadamente). Si el
legajo no existe se debe lanzar la excepción LegajoDesconocidoError (definirla adecuadamente).
Por ejemplo, con los diccionarios anteriores, el legajo 1000 y la carrera “música” se debe devolver la lista
[“piano 2”, “guitarra”, “composición”] (¿qué le falta a Ana para completar “música”?)

ii. Escribir una función guardar_carreras_en_disco que recibe un diccionario carreras y un nombre de archivo
y guarda el diccionario de carreras en el archivo indicado.
iii. Escribir una función recuperar_carreras_de_disco que recibe un nombre de archivo y recupera el
diccionario carreras a partir del archivo indicado."""

carreras = {'música': ['historia', 'piano 1', 'piano 2', 'guitarra', 'composición'],
'pintura': ['historia', 'dibujo 1', 'dibujo 2', 'pintura 1', 'pintura 2']}

alumnos = {1000: ("Ana", ["música", "pintura"], {"historia": [2, 10], "piano 1": [8], "piano 2": [2]}),
2000: ("Juan", ["pintura"], {"historia": [6], "dibujo 1": [8], "pintura 1": [2, 7], "dibujo 2": [2]})}

def que_le_falta(legajo, carrera):
	dicMaterias = {}
	materiasFaltantes = []
	if(alumnos.get(legajo, 0) is 0):
		raise LegajoDesconocidoError
	else:
		if(carreras.get(carrera, 0) is 0):
			raise CarreraDesconocidaError
		else:
			for materia in alumnos[legajo][2]:
				for y in alumnos[legajo][2][materia]:
					if y >= 4:
						dicMaterias[materia] = y
		for materias in carreras[carrera]:
			if dicMaterias.get(materias) is None:
				materiasFaltantes.append(materias)
	return materiasFaltantes

def guardar_carreras_en_disco(nombre_archivo):
	o_file = open(nombre_archivo + '.csv', 'w', newline= '')
	writer = csv.writer(o_file, delimiter=';')
	writer.writerow(('Carrera', 'Materias'))
	for x in carreras:
		writer.writerow((x, carreras[x]))
	print("Archivo creado correctamente.")
	o_file.close()

def recuperar_carreras_de_disco(nombre_archivo):
	o_file = open(nombre_archivo + '.csv', 'r')
	reader = csv.reader(o_file, delimiter=";")
	newCarreras = {}
	next(reader)
	for row in reader:
		newCarreras[row[0]] = row[1]
	print("Carreras recuperadas correctamente.")
	o_file.close()
	return newCarreras

def guardar_datos_faltan_en_disco(que_le_falta, archivo):
    ''' Ejercicio 2.ii '''
    with open(archivo, 'wb') as salida:
        pickle.dump(que_le_falta, salida)


def recuperar_datos_faltan_en_disco(archivo):
    ''' Ejercicio 2.iii '''
    with open(archivo, 'rb') as entrada:
        print(pickle.load(entrada))


		