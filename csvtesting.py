import csv

def guardar_csv():
	o_file = open('csv_entrada.csv', 'w', newline= '')
	writer = csv.writer(o_file, delimiter=';')
	writer.writerow(('Nombre', 'Telefono', 'Entrega' 'A cobrar'))
	writer.writerow(('Ana', '1566778899', 'Paquete 3 pizzas', '120'))
	writer.writerow(('Juan', '1566778800', 'Pancho y coca', '65'))
	writer.writerow(('Pepe', '1544778899', '3 empanadas', '60'))	
	o_file.close()

def leer_csv():
	i_file = open('salida.csv', 'r')
	reader = csv.reader(i_file, delimiter=';')
	reg = next(reader)
	for row in reader:
		print(row)
	i_file.close()