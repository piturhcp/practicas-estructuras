carreras = {'música': ['historia', 'piano 1', 'piano 2', 'guitarra', 'composición'],
'pintura': ['historia', 'dibujo 1', 'dibujo 2', 'pintura 1', 'pintura 2']}

alumnos = {1000: ("Ana", ["música", "pintura"], {"historia": [2, 10], "piano 1": [8], "piano 2": [2]}),
2000: ("Juan", ["pintura"], {"historia": [6], "dibujo 1": [8], "pintura 1": [2, 7], "dibujo 2": [2]})}

def que_le_falta(legajo, carrera):
	dicMaterias = {}
	materiasFaltantes = []
	if alumnos.get(legajo) is None:
		raise LegajoDesconocidoError
	else:
		if carreras.get(carrera) is None:
			raise CarreraDesconocidaError
		else:
			for materia in alumnos[legajo][2]:
				for nota in alumnos[legajo][2][materia]:
					if nota >= 4:
						dicMaterias[materia] = nota
		for materias in carreras[carrera]:
			if dicMaterias.get(materias) is None:
				materiasFaltantes.append(materias)
	return materiasFaltantes