"""Se puede utilizar como clave: Strings, enteros o tuplas. Las claves deben ser inmutables."""
diccionario = {'a' : True, 5: "Esto es un string", (1, 2): 1}
print("EJEMPLO 1:")
print(diccionario)
"""Si coloco dos claves iguales, el diccionario toma la ultima.
Imprimimos el diccionario como ejemplo, donde posee dos claves llamadas 'a'. 
Una con el valor True y otra con el valor False. Esto nos devuelve que 'a' = False"""
diccionario = {'a' : True, 5: "Esto es un string", (1, 2): 1, 'a': False}
print("\n EJEMPLO2:")
print(diccionario)
"""Se puede agregar mas claves y valores luego de creado el diccionario. 
Para eso hacemos: Ejemplo[el nombre de la clave] = el valor que le queramos dar.
Se almacena al principio del diccionario."""
diccionario["Tony Hawk"] = "Que gran tipo este Tonijok."
print("\n EJEMPLO3:")
print(diccionario)
"""Si queremos modificar el valor de una clave, lo realizamos de la misma manera que el ultimo paso.
Esto nos permite identificar que el diccionario trabaja de la siguiente forma:
Si la clave EXISTE -----> Modifica el valor.
Si la clave NO EXISTE --> Crea una clave y le asigna el valor que le damos."""
diccionario[5] = "Esto no es más un String"
print("\n EJEMPLO4:")
print(diccionario)
"""podemos recibir el valor especifico de una clave y guardarlo en una variable, o podemos imprimir
directamente el valor de la clave."""
valor = diccionario["Tony Hawk"]
print("\n EJEMPLO5:")
print(valor)
print(diccionario["Tony Hawk"])
"""Esto es inseguro, ya que si la clave no existe el codigo va a romper. En su lugar podemos utilizar el metodo GET."""
valor2 = diccionario.get("z", "Roco")
"""El metodo .get lleva dos parametros. El primero es la clave que queremos buscar, el segundo es lo que queremos
que devuelva en caso de que no exista la clave deseada. En este caso, la variable valor2 va a valer "Roco"."""
print("\n EJEMPLO6:")
print(valor2)
"""En el caso de que encuentre la clave, de vuelve el valor de la misma."""
valor3 = diccionario.get("Tony Hawk", "Roco")
print("\n EJEMPLO7:")
print(valor3)
"""Podemos eliminar utilizando la palabra reservada del e indicandole la clave que queremos eliminar."""
del diccionario[5]
print("\n EJEMPLO8:")
print(diccionario) #Vemos que la clave 5 ya no existe.
"""Podemos obtener las claves (o llaves) del diccionario con el metodo .keys. Nos devuelve un objeto iterable."""
llaves = diccionario.keys()
print("\n EJEMPLO9:")
print(llaves) 
"""Esto lo podemos convertir al formato de lista."""
llaves = list(diccionario.keys())
print("\n EJEMPLO10:")
print(llaves)
"""Se puede realizar la misma accion con los valores del diccionario. para esto utilizamos el metodo .values()"""
valores = list(diccionario.values())
print("\n EJEMPLO11:")
print(valores)
"""Si encontramos otro diccionario y queremos incorporar sus claves y valores al nuestro, podemos realizarlo mediante
el metodo .update(dic) donde el parametro dic es el diccionario que queremos que nuestro diccionario agregue."""
diccionario2 = {(1, 2): "Tienes tos.", (3, 4): "Un garabato", (5, 6): "El Cienpies", (7, 8): "El bizcocho", (9, 10): "Ya hazlo otra vez"}
diccionario.update(diccionario2)
print("\n EJEMPLO12:")
print(diccionario)
"""Como se puede ver en en los prints, nuestro diccionario añadio todo el contenido del diccionario2 sin perder el suyo,
y la clave tupla (1, 2) que ya existia en nuestro diccionario fue reemplazada por la clave tupla (1, 2): "Tienes tos."
respetando el principio de que en caso de que una clave se encuentre repetida, el diccionario siempre devuelve la ultima."""