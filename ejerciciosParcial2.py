import csv

class ErrorBase(Exception):
	#Clase base para excepciones.
	pass

class NoTieneFormatoDeLibroError(ErrorBase):
	pass

"""1. Escribir una función Python completar_datos_libro que recibe el nombre de dos archivos csv_entrada, csv_salida y un
diccionario de libros con clave el código del libro en la biblioteca y como dato una tupla (nombre del libro, autor).
El archivo CSV de entrada contiene datos de libros solicitados por los docentes para los cursos, con formato
“Docente”, “Curso”, “Codigo Libro”
“Rosita”, “Estructura Datos”, “10”
“Martín”, “Estructura Datos”, “20”
“Rosita”, “Algoritmos 2”, “5”
“Diego”, “Algoritmos 1”, “2”
La salida debe contener además los dos primeros campos (docente y materia) más el nombre del libro y el autor del libro
(o “Libro desconocido” y “Autor desconocido” si no se encuentra el libro en el diccionario).
Si el diccionario de libros es {“20”: (“Mining the Social Web”, “M. A. Russell”), “5”: (“Data structures and problem
solving using Java”, “M. A. Weiss”) } la salida será
“Docente”, “Curso”, “Nombre Libro”, “Autor”
“Rosita”, “Estructura Datos”, “Libro desconocido”, “Autor desconocido”
“Martín”, “Estructura Datos”, “Mining the Social Web”, “M. A. Russell”
“Rosita”, “Algoritmos 2”, “Data structures and problem solving using Java”, “M. A. Weiss”
“Diego”, “Algoritmos 1”, “Libro desconocido”, “Autor desconocido”"""

libros = {"20": ("Mining the Social Web", "M. A. Russell"), "5": ("Data structures and problem solving using Java", "M. A. Weiss") } 

def generarCSVparaEjercicio(nombre_archivo):
	o_file = open(nombre_archivo + ".csv", 'w', newline='')
	writer = csv.writer(o_file, delimiter=';')
	writer.writerow(('Docente', 'Curso', 'Codigo Libro'))
	writer.writerow(("Rosita", "Estructura Datos", "10"))
	writer.writerow(("Martin", "Estructura Datos", "20"))
	writer.writerow(("Rosita", "Algoritmos 2", "5"))
	writer.writerow(("Diego", "Algoritmos 1", "2"))
	o_file.close()

def completar_datos_libro(archivo_entrada, archivo_salida, dicLibros):
	i_file = open(archivo_entrada + ".csv", 'r')
	reader = csv.reader(i_file, delimiter=";")
	next(reader)
	o_file = open(archivo_salida + ".csv", 'w', newline='')
	writer = csv.writer(o_file, delimiter=';')
	writer.writerow(('Docente', 'Curso', 'Nombre Libro', 'Autor'))
	for row in reader:
		(docente, curso, codLibro) = row
		if dicLibros.get(codLibro) == none:
			nombreLibro = "Libro desconocido"
			autorLibro = "Autor desconocido"
		else:
			nombreLibro = dicLibros[codLibro][0]
			autorLibro = dicLibros[codLibro][1]
		writer.writerow((docente, curso, nombreLibro, autorLibro))
	i_file.close()
	o_file.close()

"""2. Ayudemos a la biblioteca: La biblioteca tiene un diccionario de libros, cuya clave es la tupla (nombre del libro, autor) y
cuya clave es un código. Por ejemplo, podemos tener:
libros = { (“Mining the Social Web”, “M. A. Russell”): “20”,
(“Data structures and problem solving using Java”, “M. A. Weiss”): “5” }
Se tiene además un diccionario de libros que se usan en los cursos, cuya clave es el curso y como dato una lista de libros.
Por ejemplo:
libros_en_uso = {“Estructura de datos”: [(“Mining the Social Web”, “M. A. Russell”),
(“Learning Python”, “M. Lutz”)],

“Algoritmos 1”: [(“Object-Oriented Software Construction”, “B. Meyer”)],
“Algoritmos 2”: [(“Data structures and problem solving using Java”, “M. A. Weiss”),
(“Object-Oriented Software Construction”, “B. Meyer”)]

}
i. Para ayudar a la biblioteca se debe escribir una función Python que_nos_falta que recibe los diccionarios libros y
libros_en_uso y devuelve un diccionario libros_faltan, con todos los libros que faltan como clave y la lista de las materias
que lo solicitan como contenido.
Si los datos de libros_en_uso no tienen el formato adecuado (no son una tupla de dos elementos) se debe lanzar la
excepción NoTieneFormatoDeLibroError (definirla adecuadamente).
Por ejemplo, con los diccionarios anteriores se debe devolver el diccionario
libros_faltan = {(“Learning Python”, “M. Lutz”): [“Estructura de datos”],
(“Object-Oriented Software Construction”, “B. Meyer”): [“Algoritmos 1”, “Algoritmos 2”],
ii. Escribir una función guardar_libros_en_disco que recibe un diccionario libros_faltan y un nombre de archivo y guarda
el diccionario de libros_faltan en el archivo indicado.
iii. Escribir una función recuperar_libros_de_disco que recibe un nombre de archivo y recupera el diccionario
libros_faltan a partir del archivo indicado."""

dic_libros_ej2 = { ("Mining the Social Web", "M. A. Russell"): "20", ("Data structures and problem solving using Java", "M. A. Weiss"): "5" }
libros_en_uso = {"Estructura de datos": [("Mining the Social Web", "M. A. Russell"), ("Learning Python", "M. Lutz")], 
"Algoritmos 1": [("Object-Oriented Software Construction", "B. Meyer")],
"Algoritmos 2": [("Data structures and problem solving using Java", "M. A. Weiss"), ("Object-Oriented Software Construction", "B. Meyer")]}

def que_nos_falta(libros, libros_en_uso):
	libros_faltan = {}
		for materia in libros_en_uso:
			for libroUsado in libros_en_uso[materia]:
				if len(libroUsado) is 2:
					if libros.get(libroUsado, 0) is 0:
						if libros_faltan.get(libroUsado) is None:
							libros_faltan[libroUsado] = []
							libros_faltan[libroUsado].append(materia)
						else:
							libros_faltan[libroUsado].append(materia)
				else:
					raise NoTieneFormatoDeLibroError
	return libros_faltan

def guardar_libros_en_disco(libros_que_faltan, nombre_archivo):
	o_file = open(nombre_archivo + '.csv', 'w', newline='')
	writer = csv.writer(o_file, delimiter=";")
	writer.writerow(("Libro", "Autor", "Materia"))
	for libro in libros_que_faltan:
		materias = ','.join(libros_que_faltan[libro])
		writer.writerow((libro[0], libro[1], materias))
	o_file.close()

def recuperar_libros_de_disco(nombre_archivo):
	dic = {}
	i_file = open(nombre_archivo + '.csv', 'r')
	reader = csv.reader(i_file, delimiter=";")
	next(reader)
	for row in reader:
		materias = row[2].split(',')
		dic[(row[0], row[1])] = materias
	return dic

