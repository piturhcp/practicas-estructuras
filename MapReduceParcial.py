import csv
import re

"""1 (a) Explicar en qué consiste el esquema MapReduce. (b) Usando el esquema MapReduce resolver el siguiente
problema en Python: Se tiene un archivo CSV con datos de escuelas en las que se consigna el nombre de la escuela, la
localidad y la cantidad de vacantes. Se debe devolver cuántas vacantes escolares tiene cada localidad. Por ejemplo, si el
archivo CSV contiene datos:
“Escuela”, “Localidad”, “Vacantes”
“Nro 25”, “Caseros”, 43
“Nro 32”, “Santos Lugares”, 12
“Nro 1”, “Caseros”, 5
el resultado a informar es que en Caseros hay 48 vacantes y en Santos Lugares hay 12 vacantes.
Recordar el esquema:
import csv
with open('eggs.csv') as csvfile:
spamreader = csv.reader(csvfile)
for row in spamreader:"""

"""A)"""

"""Map Reduce es un modelo para procesar grandes problemas paralelizables en muchas computadoras (nodos), 
agrupadas en un cluster (red homogenea) o en una grid (red heterogenea y distribuida geograficamente). 
Es tolerante a fallas. Consiste en dos pasos, en el primer paso (MAP), una computadora (coordinadora)
toma el problema de entrada, lo divide en subproblemas mas chicos y los distribuye entre las otras
computadoras (Asistentes). Los asistentes procesan los subproblemas mas pequeños usando la función map y 
le devuelven el resultado a la coordinadora. En el segundo paso (REDUCE), la coordinadora recolecta las
respuestas y las combina usando la función reduce para obtener el resultado buscado."""

"""B)"""

def crearCsvEntradaVacantes(archivo_entrada):
	o_file = open(archivo_entrada + '.csv', 'w', newline= '')
	writer = csv.writer(o_file, delimiter=';')
	writer.writerow(('Escuela', 'Localidad', 'Vacantes'))
	writer.writerow(('Nro 25', 'Caseros', '43'))
	writer.writerow(('Nro 32', 'Santos Lugares', '12'))
	writer.writerow(('Nro 1', 'Caseros', '5'))
	o_file.close()

def mapper(archivo_entrada, archivo_salida):
	with open (archivo_entrada + '.csv', 'r') as archivo:
		reader = csv.reader(archivo, delimiter = ';')
		next(reader)
		lista = []
		for row in reader:
			escuela, localidad, vacantes = row
			dato = (localidad, vacantes)
			lista.append(dato)
		lista.sort()
	with open (archivo_salida + '.csv', 'w', newline= '') as salida:
		writer = csv.writer(salida, delimiter = ';')
		for dato in lista:
			writer.writerow(dato)

def reducer(archivo_entrada, archivo_salida):
	with open (archivo_entrada + '.csv', 'r') as archivo:
		reader = csv.reader(archivo, delimiter = ';')
		dicVacantes = {}
		for localidad, vacantes in reader:
			if dicVacantes.get(localidad) is None:
				dicVacantes[localidad] = int(vacantes)
			else: 
				actual = dicVacantes[localidad]
				dicVacantes[localidad] = actual + int(vacantes)
	with open (archivo_salida + '.csv', 'w', newline='') as salida:
		writer = csv.writer(salida, delimiter= ';')
		for localidad in dicVacantes:
			dato = (localidad, dicVacantes[localidad])
			writer.writerow(dato)

"""2. (a) Escribir expresiones regulares que acepten solamente 
(i) Palabras / identificadores que terminan en “ch”. 
(ii) Un elemento de XML de la forma <tag> contenido </tag> (sin atributos). 
Por ejemplo, debe aceptar “<nombre>Jane Smith</nombre>” pero no “<a href="http://www.yahoo.com/">Yahoo!</a>”.
(b) Escribir en Python una función menos_uno que recibe recibe una cadena y devuelve una cadena en la cual todos los
números enteros seguidos de una palabra se reemplazan por el número encontrado menos 1. Por ejemplo
menos_uno(‘50: 5 casas 5, 3 vasos 3, 1 perro 1’) devuelve ‘50: 4 casas 5, 2 vasos 3, 0 perro 1’."""

"""(A)"""

""" i """

def aceptarSoloPalabrasQueTerminenEnCH(texto):
	expresion = ('(?:(?<=\s)|(?<=^))[a-zA-Z]*[cC][hH](?=\s|\W|$)')
	funcion = re.compile(expresion)
	res = funcion.finditer(texto)
	if res:
		for item in res:
			print(item.group())
			print(item.span())

""" ii """

def aceptarSoloTagsSinAtributos(texto):
	expresion = ('<[\w]*>.*</[\w]*>')
	funcion = re.compile(expresion)
	res = funcion.findall(texto)
	print(res)

""" iii """

def restarUno(numero):
	print(numero.group())
	v = int(numero.group(0))
	return str(v - 1)

def menos_uno(texto):
	expresion=('[\d]+(?=\s[a-zA-Z]+)')
	resultado = re.sub(expresion, restarUno, texto)
	print(resultado)


"""1 (a) Explicar en qué consiste el esquema MapReduce. (b) Usando el esquema MapReduce resolver el siguiente
problema en Python: Dado un texto devolver cuántas palabras/identificadores de 1 carácter, 2 caracteres, 3 caracteres,
..., 10 caracteres, 11 o más caracteres hay en el texto. Por ejemplo, en el texto “Hola @Jose25, como estas? Ganamos 2
medallas!" se debe devolver que hay 2 palabras de tamaño 4 (hola y como), una palabra de tamaño 5 (estas), una
palabra de tamaño 6 (jose25), una palabra de tamaño 7 (ganamos) y una palabra de tamaño 8 (medallas). (Usar una
expresión regular adecuada para separar las palabras / identificadores)."""

"""B)"""

def mapper2(cadena):
	lista = []
	cantidad = 1
	terminado = False
	while not terminado:
		if cantidad == 11:
			length = "11,"
			terminado = True
		else:
			length = str(cantidad - 1)
		expresion = ('(?:(?<=\W)|(?<=^))[a-zA-Z][\w]{' + length + '}(?=\s|$|\W)')
		funcion = re.compile(expresion)
		res = funcion.finditer(cadena)
		if res:
			for item in res:
				dato = (cantidad, item.group())
				lista.append(dato)
		cantidad = cantidad + 1
		lista.sort()
	return lista

def reducer2(lista):
	dicPalabras = {}
	for cantidad, palabra in lista:
		if dicPalabras.get(cantidad) is None:
			dicPalabras[cantidad] = []
			dicPalabras[cantidad].append(palabra)
		else:
			dicPalabras[cantidad].append(palabra)
	return dicPalabras
